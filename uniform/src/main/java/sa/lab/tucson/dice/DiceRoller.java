package sa.lab.tucson.dice;

import java.util.HashMap;
import java.util.Map;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.AbstractTucsonAgent;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.EnhancedSynchACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.core.AbstractTupleCentreOperation;

/**
 * Dice Roller exercise
 * TuCSoN agent rolling the 6-face dice on the "dice" local tuple centre
 *
 * @author s.mariani@unibo.it
 * @author giovanni.ciatto@unibo.it
 * @author andrea.omicini@unibo.it
 */

public class DiceRoller extends AbstractTucsonAgent {

    public static void main(final String[] args) {
        try {
            new DiceRoller("roller").go();
        } catch (final TucsonInvalidAgentIdException e) {
            e.printStackTrace();
        }
    }

    private boolean rolling;
    private Map<Integer, Integer> outcomes;

    public DiceRoller(final String aid) throws TucsonInvalidAgentIdException {
        super(aid);
        this.rolling = true;
        this.outcomes = new HashMap<>();
    }

    @Override
    public void operationCompleted(final AbstractTupleCentreOperation op) {}
    @Override
    public void operationCompleted(final ITucsonOperation arg0) {}

    @Override
    protected void main() {

        try {
            EnhancedSynchACC ops = getContext();
            final TucsonTupleCentreId dice = new TucsonTupleCentreId("dice","localhost", "20504");
            final LogicTuple stop = LogicTuple.parse("stop");
            int res;
            Integer resTimes = 1;
            while (this.rolling) {
                ITucsonOperation checktermination = ops.inp(dice, stop, null);
                if (checktermination.isResultSuccess()) {
                    this.rolling = false;
                    continue;
                }
                ITucsonOperation roll = ops.rd(dice, LogicTuple.parse("face(_)"), null);
                // ITucsonOperation roll = ops.urd(dice, LogicTuple.parse("face(_)"), null);
                if (roll.isResultSuccess()) {
                    res = roll.getLogicTupleResult().getArg(0).intValue();
                    resTimes = this.outcomes.get(res);
                    if (resTimes == null) {
                        this.outcomes.put(res, 1);
                    } else {
                        this.outcomes.put(res, ++resTimes);
                    }
                } else {
                    this.say("Dice rolling failed");
                }
                printStats();
                Thread.sleep(500);
                // Thread.sleep(0);
            }
        } catch (final TucsonInvalidTupleCentreIdException e) {
            e.printStackTrace();
        } catch (final InvalidLogicTupleException e) {
            e.printStackTrace();
        } catch (final TucsonOperationNotPossibleException e) {
            e.printStackTrace();
        } catch (final UnreachableNodeException e) {
            e.printStackTrace();
        } catch (final OperationTimeOutException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.say("Done rolling!");
        printStats();
    }

    private void printStats() {
        if (this.rolling) {
            this.say("Outcomes till now:");
        } else {
            this.say("Overall outcome:");
        }
        Integer sum = this.outcomes.entrySet().stream().mapToInt(Map.Entry::getValue).sum();
        for (Integer i : this.outcomes.keySet()) {
            Integer t = this.outcomes.get(i);
            this.say("\t Face " + i + " drawn " + t + " times (ratio: " + String.format("%.2f", t.floatValue() * 100 / sum.floatValue()) + "%)");
        }
    }

}
