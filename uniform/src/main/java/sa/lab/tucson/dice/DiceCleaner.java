package sa.lab.tucson.dice;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.AbstractTucsonAgent;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.BulkSynchACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.core.AbstractTupleCentreOperation;

/**
 * Dice Roller exercise
 * TuCSoN agent deleting the 6-face dice from the "dice" local tuple centre
 *
 * @author s.mariani@unibo.it
 * @author giovanni.ciatto@unibo.it
 * @author andrea.omicini@unibo.it
 */

public class DiceCleaner extends AbstractTucsonAgent {

    public static void main(final String[] args) {
        try {
            new DiceCleaner("cleaner").go();
        } catch (final TucsonInvalidAgentIdException e) {
            e.printStackTrace();
        }
    }

    public DiceCleaner(final String aid) throws TucsonInvalidAgentIdException {
        super(aid);
    }

    @Override
    public void operationCompleted(final AbstractTupleCentreOperation op) {}
    @Override
    public void operationCompleted(final ITucsonOperation arg0) {}

    @Override
    protected void main() {

        try {
            BulkSynchACC ops = getContext();
            final TucsonTupleCentreId dice = new TucsonTupleCentreId("dice","localhost", "20504");
            final LogicTuple face = LogicTuple.parse("face(_)");
            ITucsonOperation clean = ops.inAll(dice, face, null);
            if (clean.isResultSuccess()) {
                this.say("Dice cleaned up");
            } else {
                this.say("Dice cleaning failed");
            }
        } catch (final TucsonInvalidTupleCentreIdException e) {
            e.printStackTrace();
        } catch (final InvalidLogicTupleException e) {
            e.printStackTrace();
        } catch (final TucsonOperationNotPossibleException e) {
            e.printStackTrace();
        } catch (final UnreachableNodeException e) {
            e.printStackTrace();
        } catch (final OperationTimeOutException e) {
            e.printStackTrace();
        }
    }
}
