package sa.lab.tucson.dice;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.AbstractTucsonAgent;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.SynchACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.core.AbstractTupleCentreOperation;

/**
 * Dice Roller exercise
 * TuCSoN agent stopping dice rolling on the "dice" local tuple centre
 *
 * @author s.mariani@unibo.it
 * @author giovanni.ciatto@unibo.it
 * @author andrea.omicini@unibo.it
 */

public class DiceStopper extends AbstractTucsonAgent {

    public static void main(final String[] args) {
        try {
            new DiceStopper("stopper").go();
        } catch (final TucsonInvalidAgentIdException e) {
            e.printStackTrace();
        }
    }

    public DiceStopper(final String aid) throws TucsonInvalidAgentIdException {
        super(aid);
    }

    @Override
    public void operationCompleted(final AbstractTupleCentreOperation op) {}
    @Override
    public void operationCompleted(final ITucsonOperation arg0) {}

    @Override
    protected void main() {

        try {
            SynchACC ops = getContext();
            final TucsonTupleCentreId dice = new TucsonTupleCentreId("dice","localhost", "20504");
            final LogicTuple stop = LogicTuple.parse("stop");
            ITucsonOperation stopper = ops.out(dice, stop, null);
            if (stopper.isResultSuccess()) {
                this.say("Stop sent to roller");
            } else {
                this.say("Stopping roller failed");
            }
        } catch (final TucsonInvalidTupleCentreIdException e) {
            e.printStackTrace();
        } catch (final InvalidLogicTupleException e) {
            e.printStackTrace();
        } catch (final TucsonOperationNotPossibleException e) {
            e.printStackTrace();
        } catch (final UnreachableNodeException e) {
            e.printStackTrace();
        } catch (final OperationTimeOutException e) {
            e.printStackTrace();
        }
    }
}
