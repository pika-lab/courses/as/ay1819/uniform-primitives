package sa.lab.tucson.loadbalancing;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.AbstractTucsonAgent;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.BulkSynchACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.core.AbstractTupleCentreOperation;

/**
 * Service exercise
 * TuCSoN agent stopping services on the "board" local tuple centre
 *
 * @author s.mariani@unibo.it
 * @author giovanni.ciatto@unibo.it
 * @author andrea.omicini@unibo.it
 */

public class BoardCleaner extends AbstractTucsonAgent {

    public static void main(final String[] args) {
        try {
            new BoardCleaner("cleaner").go();
        } catch (final TucsonInvalidAgentIdException e) {
            e.printStackTrace();
        }
    }

    public BoardCleaner(final String aid) throws TucsonInvalidAgentIdException {
        super(aid);
    }

    @Override
    public void operationCompleted(final AbstractTupleCentreOperation op) {}
    @Override
    public void operationCompleted(final ITucsonOperation arg0) {}

    @Override
    protected void main() {

        try {
            BulkSynchACC ops = getContext();
            final TucsonTupleCentreId board = new TucsonTupleCentreId("board","localhost", "20504");
            ITucsonOperation cleanAds = ops.inAll(board, LogicTuple.parse("ad(_)"), null);
            ITucsonOperation cleanStops = ops.inAll(board, LogicTuple.parse("stop(_)"), null);
            if (cleanAds.isResultSuccess() & cleanStops.isResultSuccess()) {
                this.say("Succesfully cleaned service board");
            } else {
                this.say("Failed to clean service board");
            }
        } catch (final TucsonInvalidTupleCentreIdException e) {
            e.printStackTrace();
        } catch (final InvalidLogicTupleException e) {
            e.printStackTrace();
        } catch (final TucsonOperationNotPossibleException e) {
            e.printStackTrace();
        } catch (final UnreachableNodeException e) {
            e.printStackTrace();
        } catch (final OperationTimeOutException e) {
            e.printStackTrace();
        }
    }
}
