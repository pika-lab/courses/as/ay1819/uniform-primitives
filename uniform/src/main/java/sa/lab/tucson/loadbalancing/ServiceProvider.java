package sa.lab.tucson.loadbalancing;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.AbstractTucsonAgent;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.SynchACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.core.AbstractTupleCentreOperation;

/**
 * Dummy Service Provider class to show some 'adaptive' features related to
 * usage of uniform primitives. TuCSoN Agent composed by 2 threads: main)
 * advertises its offered service and processes incoming requests taking them
 * from a private input queue; Receiver) waits for incoming requests and puts
 * them into main thread own queue.
 *
 * @author s.mariani@unibo.it
 */
public class ServiceProvider extends AbstractTucsonAgent {

    class Receiver extends Thread {

        @Override
        public void run() {
            LogicTuple res;
            ITucsonOperation getReq;
            this.say("Waiting for requests...");
            try {
                final LogicTuple templ = LogicTuple.parse("req(" + ServiceProvider.this.service.getArg(0) + ")");
                while (ServiceProvider.this.goOn) {
                    getReq = ServiceProvider.this.ops.in(ServiceProvider.this.board, templ, null);
                    if (getReq.isResultSuccess()) {
                        ServiceProvider.this.nReqs++;
                        res = getReq.getLogicTupleResult();
                        this.say("Enqueuing request: " + res.toString());
                        /*
                         * We enqueue received request.
                         */
                        try {
                            ServiceProvider.this.inputQueue.add(res);
                        } catch (final IllegalStateException e) {
                            ServiceProvider.this.nLost++;
                            this.say("Queue is full, dropping request...");
                        }
                    }
                }
            } catch (final InvalidLogicTupleException e) {
                this.say("ERROR: Tuple is not an admissible Prolog term!");
            } catch (final TucsonOperationNotPossibleException e) {
                this.say("ERROR: Never seen this happen before *_*");
            } catch (final UnreachableNodeException e) {
                this.say("ERROR: Given TuCSoN Node is unreachable!");
            } catch (final OperationTimeOutException e) {
                this.say("ERROR: Endless timeout expired!");
            }
        }

        private void say(final String msg) {
            System.out.println("\t[Receiver]: " + msg);
        }
    }

    public static void main(final String[] args) {
        try {
            new ServiceProvider("provider1", "board@localhost:20504", 5000).go();
            new ServiceProvider("provider2", "board@localhost:20504", 3000).go();
            new ServiceProvider("provider3", "board@localhost:20504", 1000).go();
        } catch (final TucsonInvalidAgentIdException e) {
            e.printStackTrace();
        }
    }

    private static void serveRequest(final long time) {
        try {
            Thread.sleep(time);
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }
    }

    SynchACC ops;
    boolean goOn;
    /*
     * Decouples who performs the computation from who handles requests
     */
    final BlockingQueue<LogicTuple> inputQueue;
    LogicTuple service;
    /*
     * Models server performance in carrying out tasks
     */
    private final long serviceTime;
    TucsonTupleCentreId board;
    int nReqs;
    int nLost;

    /**
     * @param aid
     *            agent name
     * @param node
     *            node where to advertise services
     * @param cpuTime
     *            to simulate computational power
     *
     * @throws TucsonInvalidAgentIdException
     *             if the chosen ID is not a valid TuCSoN agent ID
     */
    public ServiceProvider(final String aid, final String board, final long cpuTime) throws TucsonInvalidAgentIdException {
        super(aid);
        this.goOn = true;
        try {
            this.board = new TucsonTupleCentreId(board);
            this.service = LogicTuple.parse("ad(" + aid + ")");
            this.say("Provider " + aid + " started");
        } catch (final TucsonInvalidTupleCentreIdException e) {
            this.say("Invalid tid given, stopping");
            this.goOn = false;
        } catch (final InvalidLogicTupleException e) {
            this.say("Invalid aid given, stopping");
            this.goOn = false;
        }
        /*
         * The upper bound models server maximum computational load
         */
        this.inputQueue = new LinkedBlockingQueue<>(10);
        this.serviceTime = cpuTime;
    }

    @Override
    public void operationCompleted(final AbstractTupleCentreOperation arg0) {}
    @Override
    public void operationCompleted(final ITucsonOperation op) {}

    @Override
    protected void main() {
        try {
            ops = getContext();
            new Receiver().start();
            ITucsonOperation op;
            LogicTuple req;
            final LogicTuple stopTuple = LogicTuple.parse("stop(" + this.myName() + ")");
            while (this.goOn) {
                this.say("Checking termination...");
                op = this.ops.inp(this.board, stopTuple, null);
                if (op.isResultSuccess()) {
                    this.goOn = false;
                    continue;
                }
                /*
                 * Service advertisement phase.
                 */
                this.say("Advertising service: " + this.service.toString());
                this.ops.out(this.board, this.service, null);
                /*
                 * Request servicing phase.
                 */
                boolean empty = true;
                try {
                    this.say("Polling queue for requests...");
                    while (empty) {
                        req = this.inputQueue.poll(1, TimeUnit.SECONDS);
                        if (req != null) {
                            empty = false;
                            this.say("Serving request: " + req.toString());
                            /*
                             * We simulate computational power of the Service
                             * Provider.
                             */
                            ServiceProvider.serveRequest(this.serviceTime);
                            /*
                             * Dummy 'positive feedback' mechanism.
                             */
                            this.say("Feedback to service: "
                                    + this.service.toString());
                            this.ops.out(this.board, this.service, null);
                        }
                    }
                } catch (final InterruptedException e) {
                    e.printStackTrace();
                }
                Thread.sleep(1000);
            }
            this.say("Provider " + this.myName() + " stopped");
            printStats();
        } catch (final InvalidLogicTupleException e) {
            this.say("ERROR: Tuple is not an admissible Prolog term!");
        } catch (final TucsonOperationNotPossibleException e) {
            this.say("ERROR: Never seen this happen before *_*");
        } catch (final UnreachableNodeException e) {
            this.say("ERROR: Given TuCSoN Node is unreachable!");
        } catch (final OperationTimeOutException e) {
            this.say("ERROR: Endless timeout expired!");
        } catch (InterruptedException e) {
            this.say("ERROR: Too early in the morning!");
        }
    }

    /**
     *
     */
    private void printStats() {
        this.say("My stats:");
        this.say("\t Total requests received: " + this.nReqs);
        this.say("\t Requests served: " + (this.nReqs - this.nLost));
        this.say("\t Requests lost: " + this.nLost);
        this.say("\t Ratio served: "
                + String.format("%.2f", Integer
                        .valueOf(this.nReqs - this.nLost).floatValue()
                        / Integer.valueOf(this.nReqs).floatValue()) + "%");
        this.say("\t Ratio lost: "
                + String.format("%.2f", Integer.valueOf(this.nLost)
                        .floatValue()
                        / Integer.valueOf(this.nReqs).floatValue()) + "%");
    }

}
