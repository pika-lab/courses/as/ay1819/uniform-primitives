package sa.lab.tucson.loadbalancing;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.AbstractTucsonAgent;
import alice.tucson.api.EnhancedSynchACC;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.core.AbstractTupleCentreOperation;

/**
 * Dummy Service Requestor class to show some 'adaptive' features related to
 * usage of uniform primitives. It probabilistically looks for available
 * services then issue a request to the Service Provider found.
 *
 * @author s.mariani@unibo.it
 */
public class ServiceRequestor extends AbstractTucsonAgent {

    /**
     * @param args
     *            no args expected.
     */
    public static void main(final String[] args) {
        try {
            new ServiceRequestor("requestor1", "board@localhost:20504").go();
            new ServiceRequestor("requestor2", "board@localhost:20504").go();
            new ServiceRequestor("requestor3", "board@localhost:20504").go();
        } catch (final TucsonInvalidAgentIdException e) {
            e.printStackTrace();
        }
    }

    private EnhancedSynchACC ops;
    private boolean goOn;
    private TucsonTupleCentreId board;

    /**
     * @param aid
     *            agent name
     * @param node
     *            node where to look for services
     *
     * @throws TucsonInvalidAgentIdException
     *             if the chosen ID is not a valid TuCSoN agent ID
     */
    public ServiceRequestor(final String aid, final String board) throws TucsonInvalidAgentIdException {
        super(aid);
        this.goOn = true;
        try {
            this.say("Requestor " + aid + " started");
            this.board = new TucsonTupleCentreId(board);
        } catch (final TucsonInvalidTupleCentreIdException e) {
            this.say("Invalid tuple centre ID given, stopping...");
            this.goOn = false;
        }
    }

    @Override
    public void operationCompleted(final AbstractTupleCentreOperation arg0) {}
    @Override
    public void operationCompleted(final ITucsonOperation op) {}

    @Override
    protected void main() {
        try {
            ops = getContext();
            final LogicTuple stopTuple = LogicTuple.parse("stop(" + this.myName() + ")");
            while (this.goOn) {
                this.say("Checking termination...");
                ITucsonOperation checktermination = this.ops.inp(this.board, stopTuple, null);
                if (checktermination.isResultSuccess()) {
                    this.goOn = false;
                    continue;
                }
                /*
                 * Service search phase.
                 */
                this.say("Looking for services...");
                ITucsonOperation getAds = this.ops.rd(this.board, LogicTuple.parse("ad(_)"), null);
                // ITucsonOperation getAds = this.ops.urd(this.board, LogicTuple.parse("ad(_)"), null);
                LogicTuple service = getAds.getLogicTupleResult();
                /*
                 * Request submission phase.
                 */
                this.say("Submitting request for service: " + service.toString());
                LogicTuple req = LogicTuple.parse("req(" + service.getArg(0) + ")");
                this.ops.out(this.board, req, null);
                Thread.sleep(1000);
            }
            this.say("Requestor " + this.myName() + " stopped");
        } catch (final InvalidLogicTupleException e) {
            this.say("ERROR: Tuple is not an admissible Prolog term!");
        } catch (final TucsonOperationNotPossibleException e) {
            this.say("ERROR: Never seen this happen before *_*");
        } catch (final UnreachableNodeException e) {
            this.say("ERROR: Given TuCSoN Node is unreachable!");
        } catch (final OperationTimeOutException e) {
            this.say("ERROR: Endless timeout expired!");
        } catch (InterruptedException e) {
            this.say("ERROR: Too early in the morning!");
        }
    }

}
