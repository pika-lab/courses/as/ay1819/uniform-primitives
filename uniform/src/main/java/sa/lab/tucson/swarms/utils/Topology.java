package sa.lab.tucson.swarms.utils;

import java.util.Properties;

/**
 * @author Stefano Mariani (mailto: s [dot] mariani [at] unibo [dot] it)
 *
 */
public final class Topology {

    /**
     *
     */
    public static void bootTopology() {
        Properties props = new Properties();
        props.put("-netid", "localhost");
        props.put("-portno", "20504");
        props.put("-tcname", "anthill");
        Boot.boot(props);

        props = new Properties();
        props.put("-netid", "localhost");
        props.put("-portno", "20505");
        props.put("-tcname", "short");
        Boot.boot(props);

        props = new Properties();
        props.put("-netid", "localhost");
        props.put("-portno", "20506");
        props.put("-tcname", "long1");
        Boot.boot(props);

        props = new Properties();
        props.put("-netid", "localhost");
        props.put("-portno", "20507");
        props.put("-tcname", "long2");
        Boot.boot(props);

        props = new Properties();
        props.put("-netid", "localhost");
        props.put("-portno", "20508");
        props.put("-tcname", "food");
        Boot.boot(props);
    }

}
