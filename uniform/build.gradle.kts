// dice

task<JavaExec>("makeDice") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sa.lab.tucson.dice.DiceBuilder"
}

task<JavaExec>("cleanDice") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sa.lab.tucson.dice.DiceCleaner"
    standardInput = System.`in`
}

task<JavaExec>("rollDice") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sa.lab.tucson.dice.DiceRoller"
    standardInput = System.`in`
}

task<JavaExec>("stopDice") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sa.lab.tucson.dice.DiceStopper"
    standardInput = System.`in`
}

// load balancing

task<JavaExec>("runServers") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sa.lab.tucson.loadbalancing.ServiceProvider"
    standardInput = System.`in`
}

task<JavaExec>("runClients") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sa.lab.tucson.loadbalancing.ServiceRequestor"
    standardInput = System.`in`
}

task<JavaExec>("stopCSAll") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sa.lab.tucson.loadbalancing.Stopper"
    standardInput = System.`in`
}

task<JavaExec>("cleanBoard") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sa.lab.tucson.loadbalancing.BoardCleaner"
    standardInput = System.`in`
}

// swarms

//// fast track
task<JavaExec>("runSwarms") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sa.lab.tucson.swarms.launchers.LaunchSwarmScenario"
    standardInput = System.`in`
}

//// step by step
task<JavaExec>("buildTopo") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sa.lab.tucson.swarms.launchers.LaunchTopology"
    standardInput = System.`in`
}

task<JavaExec>("buildEnv") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sa.lab.tucson.swarms.launchers.LaunchEnvironment"
    standardInput = System.`in`
}

task<JavaExec>("runGUI") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sa.lab.tucson.swarms.launchers.LaunchSwarmWithGUI"
    standardInput = System.`in`
}
