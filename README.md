# TuCSoN Uniform Primitives – Exercises  

## Important remarks

* Always remember to run:

    ```bash
    ./gradlew --stop
    ```

    after each demo is completed, in order to ensure all TuCSoN nodes are actually shutdown.

* You can start a TuCSoN node by running:

    ```bash
    ./gradlew tucson -Pport=<node port>
    ```

* You can start a TuCSoN CLI by running:

    ```bash
    ./gradlew cli -Pport=<node port>
    ```

* You can start a TuCSoN Inspector by running:

    ```bash
    ./gradlew inspector
    ```

## Exercise 1 – Rolling Dices (`dice` module)

* Start a TuCSoN node

    ```bash
    ./gradlew tucson
    ```
* Start a TuCSoN Inspector

  ```bash
  ./gradlew inspector
  ```

  and make it observe tuples in tuple space `dice`

* Configure the tuple space `dice`

  ```bash
  ./gradlew makeDice
  ```

  and observe its state in the Inspector

* Pick your getter primitive (`rd`/`urd`), run the roller

  ```bash
  ./gradlew rollDice
  ```

  and observe its output

* Stop the roller with

  ```bash
  ./gradlew stopDice
  ```
  and observe its final output

* Repeat the experiment with the other implementations of the roller (`rd`/`urd`)

* Clean up the tuple space `dice` with

  ```bash
  ./gradlew cleanDice
  ```

* Switch off both the Inspector and TuCSoN


## Exercise 2 – Load Balancing (`loadbalacing` module)

* Start a TuCSoN node

  ```bash
  ./gradlew tucson
  ```

* Start a TuCSoN Inspector

  ```bash
  ./gradlew inspector
  ```

  and make it observe tuples in tuple space `board`

* Switch on servers

  ```bash
  ./gradlew runServers
  ```

  and observe tuples in tuple space `board`

* Pick your getter primitive (`rd`/`urd`) on the clients, and switch them on

  ```bash
  ./gradlew runClients
  ```

  and keep observing tuples in tuple space `board`

* Stop both clients and servers with

  ```bash
  ./gradlew stopCSAll
  ```

  and observe the output

* Repeat the experiment with the other implementations of the client (`rd`/`urd`)

* Clean up the tuple space `board` with

  ```bash
  ./gradlew cleanBoard
  ```

* Switch off both the Inspector and TuCSoN

## Exercise 3 – Swarm Intelligence (`swarms` module)

### Fast track

* Run ant swarm

  ```bash
  ./gradlew runSwarms
  ```

### Step-by-step

* Generate topology

  ```bash
  ./gradlew buildTopo
  ```

  and click "Update View" to see the ants

* Configure environment

  ```bash
  ./gradlew buildEnv
  ```

* Start swarms with GUI

  ```bash
  ./gradlew runGUI
  ```

  and click "Update View" to see the ants
